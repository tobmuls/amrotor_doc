% Licensed under GPL-3.0-or-later, check attached LICENSE file

function print(self)
% Displays the object name in the Command Window
%
%    :return: Notification of object name

 disp(self.name);
end