% Licensed under GPL-3.0-or-later, check attached LICENSE file

function print(obj)
% Displays the object name in the Command Window
%
%    :param obj: Object of type Unbalance_static
%    :type obj: object
%    :return: Notification of object name

 disp(obj.name);
end