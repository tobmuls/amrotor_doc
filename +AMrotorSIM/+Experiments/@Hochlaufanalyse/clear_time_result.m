% Licensed under GPL-3.0-or-later, check attached LICENSE file

function clear_time_result(obj)
% Clears result data in object
%
%    :return: Empty result field in object

    obj.result = [];
end